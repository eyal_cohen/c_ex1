/**
 * @file StringChange.c
 * @author eyalc
 */

// ------------------------------ includes ------------------------------
#include <stdio.h>

// -------------------------- const definitions -------------------------
#define MAX_INPUT_LENGTH 50
#define ASCII_GAP_BETWEEN_BIG_A_TO_SMALL_A 32

// ------------------------------ functions -----------------------------
/**
 * simple string changing program
 */
int main()
{
    
    int inputString[MAX_INPUT_LENGTH];
    int counter = 0;
    int c = 0;
    
    // fetch input as chars and store in array
    while(counter <= MAX_INPUT_LENGTH && (c = getchar()) != '\n')
    {
        inputString[counter] = c;
        counter++;
    }
    
    printf("\"");
    
    // print input chars
    int i;
    for(i = 0; i < counter; i++)
    {
        printf("%c", inputString[i]);
    }
    
    printf("\" -> \"");
    
    // make changes and print new chars
    for(i = 0; i < counter; i++)
    {
        int currentChar = inputString[i];
        // Change rules >>
        if(currentChar >= '5' && currentChar <= '9')
        {
            currentChar = '8';
        }
        else if (currentChar >= '0' && currentChar <= '4')
        {
            currentChar = '0';
        }
        else if(currentChar >= 'A' && currentChar <= 'Z')
        {
            currentChar += ASCII_GAP_BETWEEN_BIG_A_TO_SMALL_A;
        }
        else if(currentChar >= 'a' && currentChar <= 'z')
        {
            currentChar -= ASCII_GAP_BETWEEN_BIG_A_TO_SMALL_A;
        }
        printf("%c", currentChar);
    }
    
    printf("\"\n");

    return 0;
}