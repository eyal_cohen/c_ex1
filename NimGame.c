/**
 * @file NimGame.c
 * @author  eyalc
 */

// ------------------------------ includes ------------------------------
#include <stdio.h>
#include <stdlib.h>

// -------------------------- const definitions -------------------------
#define FIRST_BOX 1
#define SECOND_BOX 2
#define FIRST_PLAYER 1
#define SECOND_PLAYER 2
#define MAX_BALLS_PER_TURN 3
#define MIN_BALLS_PER_TURN 1
#define NUM_BALLS_IN_EMPTY_BOX 0

// ------------------------------ functions -----------------------------
int getBallsNumFromUser();
void printBoard();
void printBoardHelper();
int runGame();
int getBoxIdFromUser();
int getBallsForTurnFromUser();


/**
 * Simple NimGame
 */
int main()
{
    // init vars
    int numOfBallsInBox1 = getBallsNumFromUser(FIRST_BOX);
    int numOfBallsInBox2 = getBallsNumFromUser(SECOND_BOX);
    
    // run game and get winner's id
    int winner = runGame(numOfBallsInBox1, numOfBallsInBox2);
    
    // print winning msg
    printf("Player %d wins the game.\n", winner);
    
    return 0;
}

/**
 * Run Nim Game
 *
 * Get initialized boxes and return a winner.
 *
 * @param number of balls in box 1
 * @param number of balls in box 2
 * @return winner's id
 */
int runGame(int numOfBallsInBox1, int numOfBallsInBox2)
{
    int currentPlayer = FIRST_PLAYER; // Player one plays first
    
    // loop until game ends
    while(numOfBallsInBox1 > NUM_BALLS_IN_EMPTY_BOX && numOfBallsInBox2 > NUM_BALLS_IN_EMPTY_BOX)
    {
        
        printBoard(numOfBallsInBox1, numOfBallsInBox2);
        printf("Player %d, it's your turn.\n", currentPlayer);
        
        // ask player which box
        int boxId = getBoxIdFromUser(currentPlayer);
        
        // ask player how many balls to take
        int balls = getBallsForTurnFromUser(currentPlayer, boxId, numOfBallsInBox1,
                                            numOfBallsInBox2);
        
        // make move - reduce balls from the selected box
        if(boxId == FIRST_BOX)
        {
            numOfBallsInBox1 -= balls;
        }
        else
        {
            numOfBallsInBox2 -= balls;
        }
        
        // change current player
        if(currentPlayer == FIRST_PLAYER)
        {
            currentPlayer = SECOND_PLAYER;
        }
        else
        {
            currentPlayer = FIRST_PLAYER;
        }
    }
    return currentPlayer;
}

/**
 * Ask the user how many balls to take this turn
 *
 * @param current player
 * @param chosen box
 * @param number of balls in box 1
 * @param number of balls in box 2
 * @return number of balls
 */
int getBallsForTurnFromUser(int currentPlayer, int boxId, int numOfBallsInBox1,
                            int numOfBallsInBox2)
{
    int balls;
    while(1)
    {
        printf("Player %d, how many balls do you want to take from box %d?\n", currentPlayer,
               boxId);
        scanf("%d", &balls);
        if(balls < MIN_BALLS_PER_TURN)
        {
            printf("Number of balls to take must be positive.\n");
        }
        else if(balls > MAX_BALLS_PER_TURN)
        {
            printf("Cannot take more than %d balls at a time.\n", MAX_BALLS_PER_TURN);
        }
        else if((boxId == FIRST_BOX && balls > numOfBallsInBox1) ||
                (boxId == SECOND_BOX && balls > numOfBallsInBox2))
        {
            printf("Cannot take more balls than what's in the box.\n");
        }
        else
        {
            break;
        }
    }
    return balls;
}

/**
 * Ask the user from which box to take balls this turn
 *
 * @param current player
 * @return box id
 */
int getBoxIdFromUser(int currentPlayer)
{
    int boxId;
    do
    {
        printf("Player %d, choose a box (%d or %d).\n", currentPlayer, FIRST_BOX, SECOND_BOX);
        scanf("%d", &boxId);
    }
    while(boxId != FIRST_BOX && boxId != SECOND_BOX);
    return boxId;
}

/**
 * Ask the users how many balls to put in box with boxId on initialization 
 *
 * @param box id
 * @return number of balls
 */
int getBallsNumFromUser(int boxId)
{
    int numberOfBalls = 0;
    printf("How many balls in box %d?\n", boxId);
    scanf("%d", &numberOfBalls);
    if(numberOfBalls <= 0)
    {
        printf("Number of balls in box must be positive.\n");
        exit(0);
    }
    return numberOfBalls;
}

/**
 * print board
 *
 * @param number of balls in box 1
 * @param number of balls in box 2
 */
void printBoard(int numOfBallsInBox1, int numOfBallsInBox2)
{
    printf("---------------\n");
    printBoardHelper(FIRST_BOX, numOfBallsInBox1);
    printBoardHelper(SECOND_BOX, numOfBallsInBox2);
    printf("---------------\n");
}

/**
 * helper function for printBoard
 *
 * this function prints a line representing a box
 *
 * @param number of balls in box 1
 * @param number of balls in box 2
 */
void printBoardHelper(int boxId, int numOfBallsInBox)
{
    printf("Box %d: ", boxId);
    int i;
    for(i = 0; i < numOfBallsInBox; i++)
    {
        printf("o");
    }
    printf("\n");
}
